import cPickle
from numpy.linalg import norm
from numpy import array

def writeOrgNeigborhood(extractedFileName, path_save_neighbors):
    fobj        = file(extractedFileName) #file(topFileName) 
    topData     = cPickle.load(fobj)
    fobj.close()
    topData=topData[0]
    labels      = topData["labels"]     # lables is the list of labels 
    cellNb      = len(labels)           # cellNb is the number of cells (labels) 
    barycenter  = topData["barycenter"] # barycenter a dictionary of labels and barycenters
    stem        = topData["stem"]       # stem is the labels of cells situated on the stem (i.e. barycenter[cn][cid] > a given value)
    volume      = topData["volumes"]    # volume a dictionary of labels and volumes
    L1          = topData["L1"]         #L1 is the list of labels of cells in L1 layer 
    wallSurface = topData["wall_surface"] # wallSurface is a dictionary of tuple (source, target) and the contact areas
    background_neighbors = topData["background_neighbors"] # background_neighbors[0] a set of background neighbors, the default value of background is 1
    assert 0 not in labels
    assert 1 not in labels
    labelList = list(labels)
    labelList.sort()
    label_index_Dict = [dict((lable, index) for index, lable in enumerate(labelList))]
    label_index_Dict = label_index_Dict[0]
    wallDAll = []
    wallSurfaceAll = [] 
    wallD = [] # wall diffusivity [(sourceIndex = i, targetIndex = j, A_ij / d_ij)
    wallSurfaceIndexes = []
    for edge, wallS in wallSurface.iteritems():
        sourceLabel = edge[0]
        targetLabel = edge[1]
        if sourceLabel  != 0 and sourceLabel != 1 and targetLabel != 0 and targetLabel != 1:
            wallD.append( ( label_index_Dict[sourceLabel], label_index_Dict[targetLabel], wallSurface[edge] / norm( array(barycenter[sourceLabel]) - array(barycenter[targetLabel]) ) ) )
            wallSurfaceIndexes.append( ( label_index_Dict[sourceLabel], label_index_Dict[targetLabel], wallSurface[edge] ) ) 
    wallDAll.append(wallD)
    fobj = file(path_save_neighbors, "wb")
    data2write = dict((label_index_Dict[lab], ([], [])) for lab in labels)
    for edge, wallS in wallSurface.iteritems():
        sourceIndex = label_index_Dict[edge[0]]
        targetIndex = label_index_Dict[edge[1]]        
        data2write[sourceIndex][0].append(targetIndex)
        data2write[sourceIndex][1].append(wallS)
        data2write[targetIndex][0].append(sourceIndex)
        data2write[targetIndex][1].append(wallS)
    line = str(len(data2write)) + " 1\n"
    fobj.write(line)
    for cellIndex, data in data2write.iteritems():
        line = str(cellIndex) + " " + str(len(data[0])) + " " + str(data[0])[1:-1] + " " + str(data[1])[1:-1] + "\n"
        fobj.write(line)
    fobj.close()    

if __name__ == "__main__":
    topDataFName = "/media/Seagate Expansion Drive/storage/data/Niklas/120629/B1_M1/Yassin/topData/B1_M1_0h_fused_emptySpaceRemoved_hmin_3_top.pkl"
    commonPath = "/media/Seagate Expansion Drive/storage/data/Niklas/120629/B1_M1/Yassin/"
    neighborhoodFName = topDataFName.split("/")[-1].split(".")[0] + "_organism_neighborhood.txt"
    writeOrgNeigborhood(topDataFName, commonPath + "organism/" + neighborhoodFName)

        