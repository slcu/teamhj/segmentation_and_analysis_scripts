from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
from openalea.image.all import imread, imsave
from itertools import product
from time import time
from extractTop import *
from os.path import isfile, join
from os import listdir
import scipy.ndimage 
import numpy as np
import os
import cPickle
import scipy
from writeOrganismInitFile import *
from writeOrganismNeighborsFile import *

import configFile as config
import segmentColorChange

pathfilter="/scratch/aictsgrid/tmp/mars_src/vpvt/vt_clean/vt_copie/libvp/bin/linux/cellfilter "
pathskiz="/scratch/aictsgrid/tmp/mars_src/vpvt/vt_clean/vt_copie/vt-exec/bin/linux/skiz "

def cleanSegmentation(fileName):
    #segImageName = config.commonPath + "segmentedSelected/" + fileName 
    in_name = fileName.split("/")[-1]
    cleanImagePath = "/".join(fileName.split("/")[0:-2]) + "clean/"
    cleanImageName = (cleanImagePath + in_name.split(".")[0] + "_clean_%s.tif") % (config.radius)

    print "--> Cleaned image will be saved as:", cleanImageName
    
    if not config.replace:
        if isfile(cleanImageName):
            print "The cleaned file exists, no new cleaning is done"
            return 1
    
    imsave("./segmented.inr.gz", imread(fileName))
    path_input = "./segmented.inr.gz"
    path_output = "./segmented_corrected.inr.gz"
    os.system(pathfilter + " " + path_input + " cellfilter.inr.gz -ouv -radius " + config.radius + " -chamfer")
    os.system(pathskiz + " cellfilter.inr.gz " + path_output)
    imsave(cleanImageName, imread(path_output))
    
def correctBackground(fileName):
    in_name = fileName.split("/")[-1]
    backgroundCorrectedFilePath = "/".join(fileName.split("/")[0:-2]) + "backGroundCorrected/"
    saveFileName = backgroundCorrectedFilePath + in_name.split(".")[0] + "_bk_ok.tif"
    print "--> Background corrected image will be saved as:", saveFileName
    if not config.replace:
        if isfile(saveFileName):
            print "The background corrected file exists, no new correction is done"
            return 1
    
    imageData = imread(fileName)
    largestCellLabel = config.background
    maxLabel = imageData.max()
    binCount = np.bincount(imageData.flatten())
    largestCellLabel = binCount.argmax()    
    if largestCellLabel != 1:
        np.putmask(imageData, imageData == config.background, maxLabel + 1)
        maxLabel += 1   
        np.putmask(imageData, imageData == largestCellLabel, config.background)
    imsave(saveFileName, imageData)

def calculateWalls(fileName):
    in_name = fileName.split("/")[-1]
    wallswidthPath = "/".join(fileName.split("/")[0:-2]) + "/walls/"
    wallsFileName = (wallswidthPath + in_name.split(".")[0] + "_walls_width_%d") % (config.wallsWidth)
    print "--> Calculated walls will be saved as:", wallsFileName
        
    if not config.replace:
        if isfile(wallsFileName):
            print "The walls file exists!"
            return 1
    
    tissueImageData = imread(fileName)    
    imageLaplace = scipy.ndimage.laplace(tissueImageData)
    walls = np.empty(tissueImageData.shape, dtype = bool)
    walls.fill(True)
    walls[imageLaplace == 0] = False 
    if config.wallsWidth > 2:
        walls = scipy.ndimage.morphology.binary_dilation(walls, iterations =  config.wallsWidth / 2 - 2).astype(walls.dtype)
    np.save(wallsFileName, walls)
    
def quantifySignal(segmentedImageFileName, cellWallsFileName, signalFileName, label2ExpressionValuesFile):
    cellWalls = np.load(cellWallsFileName)
    pinImage = imread(signalFileName)
    tissueImageData = imread(segmentedImageFileName)
    imageShape = tissueImageData.shape
    maxLabel = tissueImageData.max()
    backgroundVoxels = (tissueImageData == config.background)
    backgroundVoxelsDil = scipy.ndimage.morphology.binary_dilation(backgroundVoxels, iterations = surfaceVoxelsDilationIteration)
    externalVoxelsCharacteristic = (backgroundVoxelsDil - backgroundVoxels)
    externalVoxels = tissueImageData[externalVoxelsCharacteristic]
    L1LabelsSet = set(np.unique(externalVoxels))
    L1LabelsSet.update( set(np.unique(tissueImageData[..., -1])) )
    L1LabelsSet.remove(config.background) 
    L1Labels = list(L1LabelsSet)
    L1Characteristic = np.empty((maxLabel + 1, ), bool)
    L1Characteristic.fill(False)
    L1Characteristic[L1Labels] = True 
    L1CellsVoxels = L1Characteristic[tissueImageData]
    L1CellsVoxelsCopy = np.copy(L1CellsVoxels)
    L1CellsVoxelsErosion = scipy.ndimage.morphology.binary_erosion(L1CellsVoxelsCopy, iterations = L1PericlinalWallErosionIteration)
    pinImageL1CellsErosion = np.zeros(pinImage.shape, pinImage.dtype)
    pinImageL1CellsErosion[L1CellsVoxelsErosion] = pinImage[L1CellsVoxelsErosion]
    label2ExpressionValuesOnCellWall = np.zeros(maxLabel + 1)
    label2ExpressionValues = np.zeros(maxLabel + 1)
    labelVoxelCount = np.zeros(maxLabel + 1) 
    cellBorderVoxelNb = np.zeros(maxLabel + 1)
    label2ExpressionValuesOnAnticlinalWalls = np.zeros(maxLabel + 1)
    anticlinalVoxelNb = np.zeros(maxLabel + 1)
    label2ExpressionValuesOnPeriticlinalWalls = np.zeros(maxLabel + 1)
    periclinalVoxelNb = np.zeros(maxLabel + 1)
    label2ExpressionValuesOnPeriticlinalWallsExcludingSurface = np.zeros(maxLabel + 1)
    periclinalVoxelNbExcludingSurface = np.zeros(maxLabel + 1)
    for i in xrange(tissueImageData.shape[0]):
        print i
        for j in xrange(tissueImageData.shape[1]):
            for k in xrange(tissueImageData.shape[2]):
                label2ExpressionValues[tissueImageData[i, j, k]] += pinImage[i, j, k]            
                if cellWalls[i, j, k]:
                    label2ExpressionValuesOnCellWall[tissueImageData[i, j, k]] += pinImage[i, j, k]            
                    cellBorderVoxelNb[tissueImageData[i, j, k]] += 1
                    if L1CellsVoxelsErosion[i, j, k]:
    #                    label2ExpressionValuesOnAnticlinalWalls[tissueImageData[i, j, k]] += pinImageL1CellsErosion[i, j, k]
                        label2ExpressionValuesOnAnticlinalWalls[tissueImageData[i, j, k]] += pinImage[i, j, k]
                        anticlinalVoxelNb[tissueImageData[i, j, k]] += 1
                    elif L1CellsVoxels[i, j, k]:
                        periclinalVoxelNb[tissueImageData[i, j, k]] += 1
                        label2ExpressionValuesOnPeriticlinalWalls[tissueImageData[i, j, k]] += pinImage[i, j, k]
                        if not externalVoxelsCharacteristic[i, j, k]:
                            label2ExpressionValuesOnPeriticlinalWallsExcludingSurface[tissueImageData[i, j, k]] += pinImage[i, j, k]
                            periclinalVoxelNbExcludingSurface[tissueImageData[i, j, k]] += 1
    cellVoxelNb = np.bincount(tissueImageData.flatten())
    fobj = file(label2ExpressionValuesFile, "w")
    if quantifyMembraneSignal:
        cPickle.dump([label2ExpressionValuesOnCellWall, cellBorderVoxelNb, label2ExpressionValues, cellVoxelNb, label2ExpressionValuesOnAnticlinalWalls, anticlinalVoxelNb, label2ExpressionValuesOnPeriticlinalWalls, periclinalVoxelNb, label2ExpressionValuesOnPeriticlinalWallsExcludingSurface, periclinalVoxelNbExcludingSurface], fobj)
    elif quantifyCellSignal:
        cPickle.dump([label2ExpressionValues, cellVoxelNb], fobj)

    fobj.close()
    
if __name__ == "__main__":
    t1 = time()
    for commonPath in config.image_dirs:
        if config.cleanTissue:
            print "Cleaning .."
            seg_files = [ commonPath + "/segmented/" + f for f in listdir(commonPath + "/segmented/") if isfile(join(commonPath + "/segmented/", f)) ]
            for fName in seg_files:
                cleanSegmentation(fName) 

        onlyfiles = [ commonPath + "/clean/" + f for f in listdir(commonPath + "clean") if isfile(join(commonPath + "clean", f)) ]
        if config.correctBg:   
            for fName in onlyfiles:
                correctBackground(fName)

        onlyfiles1 = [ commonPath + "/backGroundCorrected/" + f for f in listdir(commonPath + "backGroundCorrected") if isfile(join(commonPath + "backGroundCorrected", f)) ] 
        if config.EightBitColors:
            for im_file in onlyfiles1:
                apply_object =  segmentColorChange.applyRandomColor()
                save_file = apply_object.initiate(im_file)
                if (not config.replace) and isfile(save_file):
                    print "The 8bit color file exists!"
                    continue
                else:
                    print "--> 8bit colorized version will be saved as", save_file
                    apply_object.execute(save_file)
                del(apply_object)

        if config.calculateCellWall:
            onlyfiles2 = [commonPath + "/walls/" + f for f in listdir(commonPath + "walls") if isfile(join(commonPath + "walls/", f))]
            for i in xrange(len(onlyfiles1)):
                if (not config.replace) and (onlyfiles1[i].split(".")[0] + "_walls_width_%d.npy"%config.wallsWidth in onlyfiles2):
                    print "The walls file exists!"
                    continue
                else:                
                    calculateWalls(onlyfiles1[i])

        if config.quantifyCellSignal or config.quantifyMembraneSignal:
            print "Extracting gene concentration .."
            onlyfiles = [ commonPath + "backGroundCorrected/" + f for f in listdir(commonPath + "backGroundCorrected") if isfile(join(commonPath + "backGroundCorrected", f)) ]    
            for i in xrange(len(onlyfiles)):
                fName = onlyfiles[i]
                segmentedImageFileName = commonPath + "backGroundCorrected/" + fName
                cellWallsFileName = commonPath + "walls/" + fName.split(".")[0] + "_walls_width_%d"%config.wallsWidth + ".npy"
                pinConcentrationFileName = commonPath + "tifs/" + imageToSignalDict[fName[:-18]+".tif"]
                label2ExpressionValuesFile = commonPath + "label2ExpressionValues/" + fName.split(".")[0] + "_walls_width_%d_erosion_%d_surfaceDil_%d.pkl"%(config.wallsWidth, L1PericlinalWallErosionIteration, surfaceVoxelsDilationIteration)
                quantifySignal(segmentedImageFileName, cellWallsFileName, pinConcentrationFileName, label2ExpressionValuesFile)

        onlyfiles = [f for f in listdir(commonPath + "backGroundCorrected") if isfile(join(commonPath + "backGroundCorrected", f))]
        cPickleFileNames = [commonPath + "topData/" + f.split(".")[0] + "_top.pkl" for f in onlyfiles]
        segmentedImages = [commonPath + "backGroundCorrected/" + f for f in onlyfiles]

        if config.extractTop:        
            onlyfiles = [commonPath + "topData/" + f for f in listdir(commonPath + "topData") if isfile(join(commonPath + "topData", f))]
            print "Extracting topology .."
            for i in xrange(len(segmentedImages)):
                if (not config.replace) and (cPickleFileNames[i] in onlyfiles):
                    print "The topology file exists!"
                    continue
                else:
                    RPObj = ReadAndcPickle([segmentedImages[i]], cPickleFileNames[i])
                    RPObj.extract()

        if config.writeOrganism:
            print "Writing organism files .."
            onlyfiles = [commonPath + "organism/" + f for f in listdir(commonPath + "organism/") if isfile(join(commonPath + "organism/", f))]
            for fname in cPickleFileNames:
                neighborhoodFName = fname.split("/")[-1].split(".")[0] + "_organism_neighborhood.txt"
                initFName = fname.split("/")[-1].split(".")[0] + "_organism_init.txt"
                if (not config.replace) and initFName in onlyfiles:
                    print "organism init file exists!"
                    continue
                else:
                    writeOrgInit(fname, commonPath + "organism/" + initFName)

                if (not config.replace) and neighborhoodFName in onlyfiles:
                    print "organism neighborhood file exists!"
                    continue
                else:
                    writeOrgNeigborhood(fname, commonPath + "organism/" + neighborhoodFName)   
        print "Time spent on postprocessing: ", time() - t1
    
    
