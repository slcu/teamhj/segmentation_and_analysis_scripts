import vtk
import time
import numpy as np
import random
import pickle
from openalea.image.all import imread
from openalea.image.algo.graph_from_image import SpatialImageAnalysis
import sys

class viewer3D(object):
    """
    This class define a three dimension viewer for any three dimensional matrix. 
    """    
    def __init__(self, imageFileName, background = 1, dataSpacing = (1, 1, 1), renBackground = (0, 0, 0)):
        self.imageData = imread(imageFileName)
        imAnalysis = SpatialImageAnalysis(self.imageData )

        print "imageData shape:", self.imageData.shape, self.imageData.flags
        self.maxLabel = self.imageData.max()
        self.background = background
        binCount = np.bincount(self.imageData.flatten())
        largestCellLabel = binCount.argmax()
        if binCount.argmax() != background:
            print "Setting background label ..."
            np.putmask(self.imageData, self.imageData == self.background, self.maxLabel + 1)
            self.maxLabel += 1   
            np.putmask(self.imageData, self.imageData == largestCellLabel, self.background)    
        self.dataSpacing = dataSpacing
        self.imageFileName = imageFileName
        self.renderer = vtk.vtkRenderer()
        self.renderWin = vtk.vtkRenderWindow()
        self.renderWin.SetWindowName("3D Viewer : " + self.imageFileName)
        self.renderWin.AddRenderer(self.renderer)
        self.renderInteractor = vtk.vtkRenderWindowInteractor()
        self.renderInteractor.SetRenderWindow(self.renderWin)
        self.renderInteractor.AddObserver("CharEvent", self.CharEventFunction)
        self.renderer.SetBackground(renBackground[0], renBackground[1], renBackground[2])
        
    def colorFunctions(self, colorLabelValues):
        self.alphaChannelFunc = vtk.vtkPiecewiseFunction()
        self.alphaChannelFunc.AddPoint(0, .01)
        self.alphaChannelFunc.AddPoint(self.background, 0)
        self.alphaChannelFunc.AddPoint(colorLabelValues[0], 0.8)
        self.alphaChannelFunc.AddPoint(colorLabelValues[1], 0.8)
        self.alphaChannelFunc.AddPoint(colorLabelValues[2], 0.8)
        self.colorFunc = vtk.vtkColorTransferFunction()
        self.colorFunc.AddRGBPoint(0, 1.0, 0.0, 0.0)
        self.colorFunc.AddRGBPoint(colorLabelValues[0], 1.0, 0.0, 0.0)
        self.colorFunc.AddRGBPoint(colorLabelValues[1], 0.0, 1.0, 0.0)
        self.colorFunc.AddRGBPoint(colorLabelValues[2], 0.0, 0.0, 1.0)
    
    def importDataCreateVolume(self, volumeData, datatype = np.uint16):
        self.dataImporter = vtk.vtkImageImport()
        self.dataImporter.SetImportVoidPointer(volumeData, volumeData.nbytes)
        if datatype == np.uint16:
            self.dataImporter.SetDataScalarTypeToUnsignedShort()
        elif datatype == np.uint8:
            self.dataImporter.SetDataScalarTypeToUnsignedChar()
            
        self.dataImporter.SetNumberOfScalarComponents(1)
        self.dataImporter.SetDataExtent(0, volumeData.shape[0] - 1, 0, volumeData.shape[1] - 1, 0, volumeData.shape[2] - 1)
        self.dataImporter.SetWholeExtent(0, volumeData.shape[0] - 1, 0, volumeData.shape[1] - 1, 0, volumeData.shape[2] - 1)
        self.dataImporter.SetDataSpacing(self.dataSpacing)
         
        colorLabelValues = [self.background + 1, volumeData.max() / 2, volumeData.max()]
        self.colorFunctions(colorLabelValues)
        
        self.volumeProperty = vtk.vtkVolumeProperty()
        self.volumeProperty.SetColor(self.colorFunc)
        self.volumeProperty.SetScalarOpacity(self.alphaChannelFunc)
         
        compositeFunction = vtk.vtkVolumeRayCastCompositeFunction()
        self.volumeMapper = vtk.vtkVolumeRayCastMapper()
        self.volumeMapper.SetVolumeRayCastFunction(compositeFunction)
        self.volumeMapper.SetInputConnection(self.dataImporter.GetOutputPort())
         
        self.volume = vtk.vtkVolume()
        self.volume.SetMapper(self.volumeMapper)
        self.volume.SetProperty(self.volumeProperty)
        
    def show(self, data = None):
        if data == None:
            self.importDataCreateVolume(self.imageData)
        else:
            self.renderer.RemoveAllViewProps()
            self.importDataCreateVolume(data, datatype = np.uint8)
        self.renderer.AddVolume(self.volume)
        self.renderInteractor.Initialize()
        self.renderWin.Render()
        self.renderInteractor.Start()
        
    def CharEventFunction(self, obj, ev):
        keypressed = self.renderInteractor.GetKeyCode()
        if keypressed == 'c':
            self.newColorMap()
        if keypressed == 's':
                fobj = file("label2ExpressionValue_crop_0h_170220.pkl", "r")
                [label2ExpressionValue, labelVoxelCount ]= pickle.load(fobj)
                fobj.close()
                ratio = label2ExpressionValue[1:] / labelVoxelCount[1:]
                print type(label2ExpressionValue)
                selectedCellsList = np.where(ratio > 10)
                selectedCellsList = list(selectedCellsList[0])
                
                #selectedCellsList = [3712, 4438, 4104, 4233, 4491, 4878, 5007, 4627, 4757, 3863, 5145, 4509, 4741, 4774, 3623, 3624, 4393, 4011, 4285, 4030, 4927, 4032, 3920, 4436, 4437, 4054, 3550, 4960, 4457, 4330, 4331, 4076, 4461, 5102, 4079, 4209, 4852, 4213, 3961]
                WUScellId = set()
                for i in xrange(len(label2ExpressionValue)):#[3712, 4165, 3624, 4393, 4330, 4076, 4079, 4209, 4436, 4437, 4438, 3863, 3961, 3550, 4213]:
                    if label2ExpressionValue[i]/ labelVoxelCount[i] > 10:
                        print label2ExpressionValue[i], labelVoxelCount[i], label2ExpressionValue[i]/ labelVoxelCount[i]
                        WUScellId.add(i)
                selectedCellsList.append(0)
                WUScellId.add(0)
                selectedCellsList = []
                for i in xrange(len(WUScellId)):
                    selectedCellsList.append(WUScellId.pop())
                    
                #WUScellId = [4032, 2306, 4331, 3716, 4438, 4104, 4233, 4491, 3724, 3714, 3726, 3215, 3217, 4627, 4757, 3863, 5145, 3098, 4509, 2501, 2981, 4774, 2983, 4393, 2602, 4011, 4741, 4285, 3506, 3295, 3261, 4030, 4927, 3008, 4878, 1989, 3790, 3922, 4436, 4437, 4054, 5007, 3551, 4960, 2660, 3557, 4457, 4330, 2155, 4076, 4461, 5102, 4079, 4209, 4852, 4213, 3320, 3194, 3708, 3325]
                #selectedCellsList = [i for i in xrange(8000, 8400)]
                self.showSelectedCells(selectedCellsList)
    
    def makeNewColormap(self, maxColorNb, differentColoredCellClusterSize, shuffle):
        colorBasketNb = maxColorNb / differentColoredCellClusterSize
        colormapList = []
        for j in xrange(differentColoredCellClusterSize):
            blockColorIndex = []
            for i in xrange(colorBasketNb):
                blockColorIndex.append( i * differentColoredCellClusterSize + j + 2)
                if shuffle:
                    random.shuffle(blockColorIndex)
            colormapList.extend(blockColorIndex)
        colormapBase = np.array(colormapList, dtype = np.uint8)
        colormap = np.array(colormapBase, dtype = np.uint8, order = 'F')
        for i in xrange(int(np.ceil(self.maxLabel / (maxColorNb))) + 1):
            colormap = np.hstack([colormap, colormapBase])
        self.colormap = np.hstack([np.array([0, 1], dtype = np.uint8), colormap])
        print self.colormap.flags
        
    
    def showSelectedCells(self, selectedCellsList, maxColorNb = 128, differentColoredCellClusterSize = 8, shuffle = True):        
        print "Displaying selected cells ..." 
        selectedCellsColormap = np.empty(self.maxLabel + 1, dtype = np.uint8)
        selectedCellsColormap.fill(self.background)
        try:
            selectedCellsColormap[selectedCellsList] = self.colormap[selectedCellsList]
        except AttributeError:
            self.makeNewColormap(maxColorNb, differentColoredCellClusterSize, shuffle)
            selectedCellsColormap[selectedCellsList] = self.colormap[selectedCellsList]
        colorLabelValues = [self.background + 1, maxColorNb / 2 + 1, maxColorNb + 2] 
        self.colorFunctions(colorLabelValues)
        self.colorIndexImage = np.array(selectedCellsColormap[self.imageData], order = 'F' )# Attention: data must be np.uint8c
        self.show(self.colorIndexImage)        
                      
            
    def newColorMap(self, maxColorNb = 128, differentColoredCellClusterSize = 8, shuffle = True):
        print "Wait while the new color map is being applied ..."
        self.makeNewColormap(maxColorNb, differentColoredCellClusterSize, shuffle)        
        colorLabelValues = [self.background + 1, maxColorNb / 2 + 1, maxColorNb + 2] 
        self.colorFunctions(colorLabelValues)
        self.colorIndexImage = np.array(self.colormap[self.imageData], order = 'F') # Attention: data must be np.uint8
        print "newColorMap", self.colorIndexImage.flags
        self.show(self.colorIndexImage)
        
if __name__ == "__main__":     
    viewer = viewer3D(sys.argv[1], dataSpacing = (1, 1, 1.), background = 1)
    viewer.show()
    
    
    
                