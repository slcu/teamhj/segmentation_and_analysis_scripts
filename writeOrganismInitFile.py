import cPickle
import numpy as np

def writeOrgInit(extractedFileName, path_save_init):
    fobj        = file(extractedFileName) #file(topFileName) 
    topData     = cPickle.load(fobj)
    fobj.close()
    topData=topData[0]
    labels      = topData["labels"]     # lables is the list of labels 
    cellNb      = len(labels)           # cellNb is the number of cells (labels) 
    barycenter  = topData["barycenter"] # barycenter a dictionary of labels and barycenters
    stem        = topData["stem"]       # stem is the labels of cells situated on the stem (i.e. barycenter[cn][cid] > a given value)
    volume      = topData["volumes"]    # volume a dictionary of labels and volumes

    L1          = topData["L1"]         #L1 is the list of labels of cells in L1 layer 
    wallSurface = topData["wall_surface"] # wallSurface is a dictionary of tuple (source, target) and the contact areas
    background_neighbors = topData["background_neighbors"] # background_neighbors[0] a set of background neighbors, the default value of background is 1
    assert 0 not in labels
    assert 1 not in labels
    labelList = list(labels)
    labelList.sort()
    label_index_Dict = [dict((lable, index) for index, lable in enumerate(labelList))]
    label_index_Dict = label_index_Dict[0]
    
    option_list = []
    
    gene1_expressed_in_cell = L1
    option_list.append(gene1_expressed_in_cell)
    
    gene2_expressed_in_cell = []
    option_list.append(gene2_expressed_in_cell)
    
    gene3_expressed_in_cell = [] # values for testing
    option_list.append(gene3_expressed_in_cell)
    
    gene4_expressed_in_cell = []
    option_list.append(gene4_expressed_in_cell)
    
    gene5_expressed_in_cell = []
    option_list.append(gene5_expressed_in_cell)
    
    gene6_expressed_in_cell = []
    option_list.append(gene6_expressed_in_cell)
    
    gene7_expressed_in_cell = []
    option_list.append(gene7_expressed_in_cell)
    
    gene8_expressed_in_cell = []
    option_list.append(gene8_expressed_in_cell)
    
    fobj = file(path_save_init, "wb")
    line = str(len(labels)) + " " + str(4 + len(option_list)) + "\n" 
    fobj.write(line)
    for index in xrange(len(labelList)):
        lab = labelList[index]
        line = str( barycenter[lab][0]) + " " + str(barycenter[lab][1]) + " " + str(barycenter[lab][2]) + " "
        line += str( volume[lab]) + " "
        
    
        for i in range(len(option_list)):
            if lab in option_list[i]:
                line += "1 "
            else:
                line += "0 "
        line += "\n"
        fobj.write(line)
    fobj.close()    
    

if __name__ == "__main__":
    topDataFName = "/media/Seagate Expansion Drive/storage/data/Niklas/120629/B1_M1/Yassin/topData/B1_M1_0h_fused_emptySpaceRemoved_hmin_3_top.pkl"                   
    commonPath = "/media/Seagate Expansion Drive/storage/data/Niklas/120629/B1_M1/Yassin/"
    initFName = topDataFName.split("/")[-1].split(".")[0] + "_organism_init.txt"
    writeOrgInit(topDataFName, commonPath + "organism/" + initFName)    
