#
# TODO:
# Remove need of writing down file names (check folders instead)
#

import sys
from os import listdir
from os.path import isfile, join

__doc__ = """


#===============================================================================
# directories and files path
#===============================================================================


commonPath: The path to the directory where files are saved

files2SegmentList : list of the files in the tifs directory to be segmented
segmentedSelectedFileList : List of files in the segmentedSelected directory  
signalQuantificationFileNameList: List of files in which signal will be quantified, note their order should correspond to the order of file in segmentedSelectedFileList  

#===============================================================================
# flags
#===============================================================================

#===============================================================================
## General boolean flags
#===============================================================================

 
all: if True, the images will be fused and then segmented with all h_min values and then all possible pro cesses will be applied to them

base: If True, no fusion is done and the first value in h_min list will be used to segment the data #have all

fuse : if True the different angles will be fused
cleanTissue : if True, the tissue will be cleaned
quantifyCellSignal : if True, the signal in the cells will be extracted and pickled 
quantifyMembraneSignal : if True, the signal on the cell membrane will be extracted and pickled 
segment : if True, the files will be segmented
correctBg : if True, the background is set to 1
extractTop : if True, the topological information about the segmented images is extracted
writeOrganism : if True, organism init and neighborhood files will be created
view : if True, the files will be viewed after segmentation
replace: if True, the existing files will be overwritten, to make sure that the same process will not be applied several times 
calculateCellWall: if True, the cell walls will be extracted

#===============================================================================
# parameters
#===============================================================================

radius : the parameter used in the cleaning algorithm
background : the label of background in the segmented image
wallsWidth : the cell wall width (#Voxels) in the extracted cell walls
L1PericlinalWallErosionIteration : parameter used the calculate cell walls 
surfaceVoxelsDilationIteration : parameter used the calculate cell walls
h_minList : H_min values list used in the segmentation

"""


if __name__ == "__main__":
    if len(sys.argv) >= 2 and sys.argv[1] == "help":
        print __doc__
        exit()
    configFileName = sys.argv[1]
    fobj = file("configFile.py", "wb")
    fobj.write("from %s import *"%configFileName.split(".")[0])    
    fobj.close()
    import configFile
    import MARSSegmentation
    from viewer3d import *    
    execfile("MARSSegmentation.py")
    #raw_input("segmentation is done, copy the suitable segmented files to the segmentedSelected directory, set the signal file in config file, then press Enter")
    import postSegmentation
    execfile("postSegmentation.py")        
    if config.view:
        onlyfiles = [ f for f in listdir(commonPath + "/backGroundCorrected/") if isfile(join(commonPath + "/backGroundCorrected/", f)) ]
        for fname in onlyfiles:
            viewer = viewer3D(commonPath + "/backGroundCorrected/" + fname, dataSpacing = (1, 1, 1.), background = background)
            viewer.show()
            
    
