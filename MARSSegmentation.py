from libtiff import *
from openalea.image.algo.analysis import SpatialImageAnalysis
from openalea.image.algo.basic import logicalnot
from openalea.image.all import imread, imsave, point_selection
from openalea.image.spatial_image import SpatialImage
from scipy import ndimage
from time import time
from vplants.asclepios.vt_exec.connexe import hysteresis, connected_components
from vplants.asclepios.vt_exec.morpho import dilation, erosion
from vplants.asclepios.vt_exec.recfilters import recfilters
from vplants.asclepios.vt_exec.regionalmax import regionalmax
from vplants.asclepios.vt_exec.watershed import watershed
from vplants.mars_alt.mars import segmentation
from vplants.mars_alt.mars.all import automatic_linear_parameters, \
    automatic_non_linear_parameters, fuse_reconstruction, reconstruct, \
    reconstruction_task, surface_landmark_matching_parameters
from vplants.mars_alt.mars.reconstruction import im2surface, surface2im
import numpy as np
import os
import sys
# from tiffFileRes import getTiffFileResolution
import configFile as config
from os.path import isfile, join
from os import listdir

def fuseImages(im0Name, im1Name, im2Name, mannualInitialize = False):
    in_name = im0Name.split("/")[-1]
    fusedImagePath = "/".join(im0Name.split("/")[0:-2]) + "/fused/"
    fusedImageName = fusedImagePath + in_name.split(".")[0] + "_fused.tif"
    if not config.replace:
        if isfile(fusedImageName):
            print "The fused file exists, no new fusion is done"
            return 1
    im0 = imread(im0Name)
    im1 = imread(im1Name)
    im2 = imread(im2Name)

    print "Images being fused:", im0Name, im1Name, im2Name
    print "--> Fused image will be saved as:", fusedImageName

    #---------------------------------------------------------------------------------
    if mannualInitialize:
        # -- create projection images --
        surf0,alt0 = im2surface(im0)
        surf1,alt1 = im2surface(im1)
        surf2,alt2 = im2surface(im2)
        
        # -- create point sets --
        ps0 = point_selection(surf0)
        ps1 = point_selection(surf1)
        # -- Recover 2D coordinates of the points:
        pts01_0=ps0.get_points()
        pts01_1=ps1.get_points()
        np.savetxt("p01-0.txt", pts01_0)
        np.savetxt("p01-1.txt", pts01_1)
        
        # -- create point sets --
        ps0 = point_selection(surf0)
        ps2 = point_selection(surf2)
        # -- Recover 2D coordinates of the points:
        pts02_0=ps0.get_points()
        pts02_2=ps2.get_points()
        np.savetxt("p02-0.txt", pts02_0)
        np.savetxt("p02-2.txt", pts02_2)
        
        # -- Point set paths --
        #pt_set_names = "P1_0h_p01-0.txt", "P1_0h_p01-1.txt", "P1_0h_p02-0.txt", "P1_0h_p02-2.txt",
        pt_set_names = "p01-0.txt", "p01-1.txt", "p02-0.txt", "p02-2.txt",
        pt_set_paths = "./"
        
        pts01_0, pts01_1, pts02_0, pts02_2 = [np.loadtxt(f) for f in pt_set_names]
        
        #########################################################
        # -- Recover REAL coordinates of the points:
        pts01_0_3D = surface2im((pts01_0), alt0)
        pts01_1_3D = surface2im((pts01_1), alt1)
        pts02_0_3D = surface2im((pts02_0), alt0)
        pts02_2_3D = surface2im((pts02_2), alt2)
    
    
    ###################################
    # Image 1 on image 0 registration #
    ###################################
    
    # -- Parametrise the landmark registration --
    
    if mannualInitialize:
        ldmark_params_01 = surface_landmark_matching_parameters(pts01_0_3D, pts01_1_3D)
    else:
        ldmark_params_01 = None
    
    # -- Parametrise the auto-linear registration --
    
    auto_lin_params = automatic_linear_parameters(transfo_type='affi', estimator='ltsw',
                                                  pyramid_levels=6, finest_level=2)
    
    # -- Parametrise the auto-non-linear registration --
    
    auto_nonlin_params = None #automatic_non_linear_parameters(start_level=3, end_level=1, threads=8)
    
    # -- Create the reconstruction task that registers image 1 on image 0 --
    
    recon_task0_0 = reconstruction_task(im0, im0, initialisation = ldmark_params_01,
                                        auto_linear_params=None,
                                        auto_non_linear_params=None)
    
    recon_task0_1 = reconstruction_task(im0, im1, initialisation = ldmark_params_01,
                                        auto_linear_params=auto_lin_params,
                                        auto_non_linear_params=auto_nonlin_params)
    
    ###################################
    # Image 2 on image 0 registration #
    ###################################
    
    # -- Parametrise the landmark registration --
    
    if mannualInitialize:
        ldmark_params_02 = surface_landmark_matching_parameters(pts02_0_3D, pts02_2_3D)
    else:
        ldmark_params_02 = None
    
    # -- Create the reconstruction task that registers image 2 on image 0 --
    
    recon_task0_2 = reconstruction_task(im0, im2, initialisation = ldmark_params_02,
                                        auto_linear_params=auto_lin_params,
                                        auto_non_linear_params=auto_nonlin_params)
    
    
     
            # -- This will take quite more time! --

    (recons_task, recons_results) = reconstruct([recon_task0_1, recon_task0_2])
    fused_im_0_1_2 = fuse_reconstruction(recons_task, recons_results)
    imsave(fusedImageName, SpatialImage(fused_im_0_1_2))

def segmentFunc(fName, h_min, fromFused = False):    
    in_name = fName.split("/")[-1]
    segImagePath = "/".join(fName.split("/")[0:-2]) + "/segmented/"
    segImageName = segImagePath + in_name.split(".")[0] + "_hmin_%d.tif" % (h_min)
    print "--> Segmented image will be saved as", segImageName
    if not config.replace:
        if isfile(segImageName):
            print "The segmented file exists, no new segmentation is done"
            return 1
    

    im0 = imread(fName)  
    if config.getTifRes:
        originalFileResolution = getTiffFileResolution(im0Name)
        im0.resolution = originalFileResolution
    im_filtered = segmentation.filtering(im0, filter_type="asf", filter_value=3)
    im_tmp = logicalnot(im_filtered)
    im_tmp = regionalmax(im_tmp, h_min)
    im_tmp = hysteresis(im_tmp, 1, h_min, connectivity=6)
    seeds = connected_components(im_tmp, 1)
    seg = watershed(seeds, im_filtered)
    if config.getTifRes:
        seg.resolution = originalFileResolution
    imsave(segImageName, seg)
    
if __name__ == "__main__":
    for commonPath in config.image_dirs:
        if config.fuse:
            toFuse_files = [ commonPath + "tifs/" + f for f in listdir(commonPath + "tifs") if isfile(join(commonPath + "tifs/", f)) ]
            try:
                fuseImages(toFuse_files[0], toFuse_files[1], toFuse_files[2])
            except Exception as exc:
                print "ERROR when fusing:", exc
            fused_image = [ commonPath + "fused/" + f for f in listdir(commonPath + "fused") if isfile(join(commonPath + "fused/", f)) ]
            if config.segment:
                for h_min in config.h_minList:
                    for fused in fused_image:
                        segmentFunc(fused, h_min, fromFused = True)
        elif config.segment:
            tifs_files = [ commonPath + "tifs/" + f for f in listdir(commonPath + "tifs") if isfile(join(commonPath + "tifs/", f)) ]
            for fName in tifs_files:
                for h_min in config.h_minList:
                    #print "file name  hmin", fName, h_min
                    segmentFunc(fName, h_min)
