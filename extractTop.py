import cPickle
from numpy import array
from time import time, sleep
import numpy as np
from openalea.image.serial.basics import imread
from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from configFile import *
# from tiffFileRes import getTiffFileResolution

"""
This module contains a class with methods to extract the data needed to simulate a gene network model from a set of segmented images 
"""

class ReadAndcPickle(object):
    """
    Read a list of images, extract and cPickle volumes, barycenters of cells, contact areas between cells, adjacency graphs, L1 layer, and cells on the stem.  
    """
    
    def __init__(self, fileNames, cPickleFileName, background = 1):
        self.fileNames = fileNames
        self.cPickleFileName = cPickleFileName
        self.realZStemCriterionList = []
        for fname in fileNames:
            print "File name = ", fname
            self.realZStemCriterionList.append(0)
#             self.realZStemCriterionList.append(float(raw_input("Enter z coordinate below which the cells are on the stem:")))        
        self.background = background
        
    def getResolution(self, fname):
        print "Enter voxel sizes for the image ", fname
        if(len(v_size_list) != 3):
            xVSize = float(raw_input("Enter voxel size X: "))
            yVSize = float(raw_input("Enter Y voxel size Y: "))
            zVSize = float(raw_input("Enter Z voxel size Z: "))
            return [xVSize, yVSize, zVSize]
        else:
            return v_size_list
    
    def readImages(self):
        """
        Read images using imread from openalea.image package
        
        :Returns:
        -    self.readImages    - List of read images 
        """
        self.readIms = [imread(fName) for fName in self.fileNames]
        self.resolutions = [self.getResolution(fName) for fName in self.fileNames]
#        self.resolutions = [[0.26, 0.26, 0.26] for fName in self.fileNames]
        for i in xrange(len(self.readIms)):
            print self.resolutions[i]
            self.readIms[i].resolution = self.resolutions[i] 
        return self.readIms
    
    def analyze(self):
        """
        Extract property graphs from read images
        
        :Returns:
        -    self.pGraphs    -    List of extracted property graphs
        """
        self.readImages()
        self.imAnalysis = [SpatialImageAnalysis(im) for im in self.readIms]
        return self.imAnalysis
    
    def extractFromAnalysis(self):
        """
        Read extracted property graphs from self.pGraphs and extract a dictionary of volumes, barycenters, contact areas, adjacency graphs, L1 layer, and cells on the stem.
        
        :Returns:
        
        -    self.topData    -    List of dictionaries of volumes, barycenters, contact areas, adjacency graphs, L1 layer, cells on the stem
        """
        self.topData = []
        for counter, im_analysis in enumerate(self.imAnalysis):
            labels = list(im_analysis.labels())
            if self.background in labels : del labels[labels.index(self.background)]
            labelset = set(labels)
            neighborhood = im_analysis.neighbors(labels) # neighborhood is a dictionary of cell labels and the list of corresponding neighbors i.e. {cellLable: [NeighborCellLable_1, ...], ..} 
            print "neighborhood is extracted"
            edges = []
            for source, targets in neighborhood.iteritems():
                if source in labelset:
                    for target in targets:
                        if source < target and target in labelset:
                            edges.append((source, target))
            edgesDict = dict((source, []) for source, target in edges)
            for source, target in edges:
                edgesDict[source].append(target)
            print "edges extracted"    
            barycenter = dict( zip( labels, im_analysis.center_of_mass(labels, real = True) ) )
            print "barycenter extracted"
            volumes = dict( zip( labels, im_analysis.volume(labels, real = True) ) )
            print "volumes extracted"
            wall_surface = im_analysis.wall_surfaces(edgesDict, real = True)
            print "wall surface extracted"
            background_neighbors = set(im_analysis.neighbors(self.background))
            background_neighbors.intersection_update(labelset)            
            L1 = [lable for lable in background_neighbors if barycenter[lable][2]  >= self.realZStemCriterionList[counter] ]
            print "L1 length: ", len(L1)  
            stem = [lable for lable in labels if barycenter[lable][2] < self.realZStemCriterionList[counter] ]  
            self.topData.append({"labels": labels, "volumes" : volumes, "barycenter" : barycenter, "L1": L1, "wall_surface": wall_surface, "stem": stem, "background_neighbors":background_neighbors})
        return self.topData
    
    def cPickleTop(self):
        """
        Dump the list of dictionaries  
        """
        fobj = file(self.cPickleFileName, "wb")
        cPickle.dump(self.topData, fobj)
        fobj.close()
    
    def extract(self):
        self.analyze()
        self.extractFromAnalysis()
        self.cPickleTop()
        
if __name__ == "__main__":
        segmentedImage = "/media/Seagate Expansion Drive/storage/data/Niklas/120629/B1_M1/Yassin/segmented/B1_M1_0h_fused_emptySpaceRemoved_hmin_3.tif"
        cPickleFileName = "/media/Seagate Expansion Drive/storage/data/Niklas/120629/B1_M1/Yassin/topData/B1_M1_0h_fused_emptySpaceRemoved_hmin_3_top.pkl"
        RPObj = ReadAndcPickle([segmentedImage], cPickleFileName)
        RPObj.extract()

