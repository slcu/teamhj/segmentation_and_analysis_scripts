import os
import sys
from numpy import *
from random import randint
import warnings
import configFile as config
with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    from openalea.image.all import imread, imsave
    from openalea.image.spatial_image import SpatialImage

class applyRandomColor:
    """
    Loops through all the voxels in a segmented image and applies a color 
    (value between 0 and 255) for all the voxels.
    Faster than previous approach but does not check walls.
     """
    def initiate(self, read_file):       
        in_name = read_file.split("/")[-1]
        path = "/".join(read_file.split("/")[0:-2]) + "/8bit/"
        save_file = path + in_name.split(".")[0] + "_8bitColor.tif"
        self.seg = imread(read_file)
        self.seg = array(self.seg)
        
        return save_file
    
    def execute(self, save_file):
        new_image = self.seg % 255 - 1
        new=SpatialImage(new_image, dtype='int8')
        imsave(save_file, new)
        return
        
        
def printHelp():
    import textwrap
    wrap=textwrap.TextWrapper(replace_whitespace=False, width=80, drop_whitespace=True, initial_indent='\n')
    helpFile = open("%s/help/%sHelp.txt"%(os.path.dirname(os.path.realpath(__file__)), os.path.basename(__file__)[:-3]), "r")    
    for line in helpFile:
        for i in wrap.wrap(line):
            print i
            
    helpFile.close()    
    exit()
    return

if __name__=="__main__":
    if sys.argv[1] in ["-h", "--help"]:
        printHelp()
    else:
        apply_object = applyRandomColor()
        save_file = apply_object.initiate(sys.argv[1])
        apply_object.execute( save_file)

