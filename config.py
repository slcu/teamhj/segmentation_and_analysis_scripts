import os

#===============================================================================
# directories and files path
#===============================================================================


image_dirs = ["/scratch/aictsgrid/tmp/andre/test_data_october/membrane/P1_0h/",
              "/scratch/aictsgrid/tmp/andre/test_data_october/membrane/P1_19h/"] # The path to the directory where files are saved
v_size_list = [0.19, 0.19, 1] # voxel sizes

# These two lists are only used if quantifyCellSignal is True
segmentedSelectedFileList = ["membrane_AHK3_YPT_R_0fused_hmin_4.tif"] # List of files in the segmentedSelected directory  
signalQuantificationFileNameList = [] # List of files in which signal will be quantified, note their order should correspond to the order of file in segmentedSelectedFileList  

#===============================================================================
# flags
#===============================================================================

#===============================================================================
## General flags
#===============================================================================

 
all = False # if True, the images will be fused and then segmented with all h_min values and then all possible pro cesses will be applied to them
base = False # If True, no fusion is done and the first value in h_min list will be used to segment the data #have all

if all and base:
    raise ValueError("Both all and base flags can not be True at the same time!")
    
fuse = False # if True the different angles will be fused
cleanTissue = True # if True, the tissue will be cleaned
quantifyCellSignal = False # if True, the signal in the cells will be extracted and pickled 
quantifyMembraneSignal = False # if True, the signal on the cell membrane will be extracted and pickled 
segment = True # if True, the files will be segmented
correctBg = True # if True, the background is set to 1
extractTop = True # if True, the topological information about the segmented images is extracted
writeOrganism = True # if True, organism init and neighborhood files will be created
view = False # if True, the files will be viewed after segmentation
replace = False # if True, the existing files will be overwritten, to make sure that the same process will not be applied several times 
calculateCellWall = True # if True, the cell walls will be extracted
viewImageJ = True
EightBitColors = True # Create stack of images where cells are given repeating 8bit color values.

#===============================================================================
# parameters
#===============================================================================

radius = "3" # the parameter used in the cleaning algorithm
background = 1 # the label of background in the segmented image
wallsWidth = 2 # the cell wall width (#Voxels) in the extracted cell walls
L1PericlinalWallErosionIteration = 1 # parameter used the calculate cell walls 
surfaceVoxelsDilationIteration = 2 # parameter used the calculate cell walls
h_minList = [3,4,5] # H_min values used in the segmentation
getTifRes = False # if True, the voxels sizes will be extracted from tiff files



#===============================================================================
# Setting the flags and creating the directories, you do need to modify this part
#===============================================================================

if all:
    fuse = False
    cleanTissue = True
    segment = True
    correctBg = True
    extractTop = True
    writeOrganism = True
    writeOrganism = True
    calculateCellWall = True
if base:
    fuse = False
    cleanTissue = True
    segment = True
    correctBg = True
    extractTop = True
    writeOrganism = True
    writeOrganism = True
    calculateCellWall = True


#===============================================================================
### Creating files and directories
#===============================================================================
directoryNames = ["segmented", "segmentedSelected", "clean", "walls", "label2ExpressionValues", "backGroundCorrected", "organism", "topData", "fused", "8bit"]
if quantifyCellSignal or quantifyMembraneSignal:
    imageToSignalDict = dict((segmentedSelectedFileList[i], signalQuantificationFileNameList[i]) for i in xrange(len(segmentedSelectedFileList)))
for commonPath in image_dirs:
    for dir in directoryNames:
        directory = commonPath + dir 
        if not os.path.exists(directory):
            print directory , " is created."
            os.makedirs(directory)
